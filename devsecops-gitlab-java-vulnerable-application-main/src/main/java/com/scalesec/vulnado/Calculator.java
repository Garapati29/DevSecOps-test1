package com.scalesec.vulnado;

public class Calculator {
    public int addition(String expression) {
        // Dummy implementation for addition
        String[] parts = expression.split("\\+");
        int num1 = Integer.parseInt(parts[0]);
        int num2 = Integer.parseInt(parts[1]);
        return num1 + num2;
    }
}
